#region usings
using System;
using System.ComponentModel.Composition;
using System.Globalization;
using System.Collections.Generic;
using System.Collections;
using VVVV.PluginInterfaces.V1;
using VVVV.PluginInterfaces.V2;
using VVVV.Utils.VMath;
using VVVV.Utils.OSC;

using VVVV.Core.Logging;
#endregion usings

namespace VVVV.Nodes
{



	#region PluginInfo
	[PluginInfo(Name = "CompleteOSCMessage", Category = "Iris", Help = "Basic Node to manage the Vertigo Interface along with the OSC-Messages", Tags = "Iris, v4Bundle, Parameter")]
	#endregion PluginInfo

	public class IrisCompleteOSCMessageNode : IPluginEvaluate
	{

		#region fields & pins
	

		[Input("OSCMessages", DefaultString = "MyOSCMessage")]
		IDiffSpread<string> FOSCMessages;

		[Input("TypeTag")]
		IDiffSpread<string> FTypeTag;
		
		[Input("Expected IOType")]
		IDiffSpread<string> FExpectedIOType;
		
//		[Input("Local State")]
//		ISpread<bool> FLocalState;

//-------------------------------------



		[Output("Unmodiefied Messages")]
		ISpread<string> FUnmodifiedOSCMessages;
		
		[Output("Modified OSC Address")]
		ISpread<string> FModifiedOSCAdress;
		
		[Output("Modified OSC Values")]
		ISpread<string> FModifiedValues;
		
		
		
		[Import()]
		IPluginHost FHost;
		[Import()]
		ILogger Flogger;

		#endregion fields & pins
		
		List<string> UnmodfifiedOSCMessages = new List<string>();
		List<string> ModifiedOSCAdress = new List<string>();
		List<string>ModifiedValues = new List<string>();
		string localstate="1";
		private OSCReceiver FOSCReceiver;


		private byte[] StringToByteArray(string str)
		{
			return System.Text.Encoding.GetEncoding(1252).GetBytes(str);
		}

		private void CompleteOSCMessage(int myindex, string iotype)
		{
				string mymessage;
				
				OSCPacket packet = OSCPacket.Unpack(StringToByteArray(FOSCMessages[myindex]));
				{
					ModifiedOSCAdress.Add(packet.Address);
					if (packet != null)
					{
							ArrayList messages = packet.Values;
							switch (iotype)
							{
								case "Bang":	
									ModifiedValues.Add("0"); //X
									ModifiedValues.Add("0"); //Y
									ModifiedValues.Add(Convert.ToString(messages[0])); //Z
									ModifiedValues.Add(localstate);
									break;
								case "Toggle":	
									ModifiedValues.Add("0"); //X
									ModifiedValues.Add("0"); //Y
									ModifiedValues.Add(Convert.ToString(messages[0])); //Z
									ModifiedValues.Add(localstate);
									break;
								case "X":
									mymessage=Convert.ToString(messages[0]);
									mymessage=mymessage.Replace(',', '.');
									ModifiedValues.Add(mymessage); //X
									ModifiedValues.Add("0"); //Y
									ModifiedValues.Add("0"); //Z
									ModifiedValues.Add(localstate);
									break;
								case "XY":
									mymessage=Convert.ToString(messages[0]);
									mymessage=mymessage.Replace(',', '.');
									ModifiedValues.Add(mymessage); //X
									if (messages.Count>1)
										mymessage=Convert.ToString(messages[1]);
									else
										mymessage=Convert.ToString(messages[0]);
									mymessage=mymessage.Replace(',', '.');
									 ModifiedValues.Add(mymessage); //Y
									ModifiedValues.Add("0"); //Z
									ModifiedValues.Add(localstate);
									break;
								case "Color(H+LasXYValue)":
									mymessage=Convert.ToString(messages[0]);
									mymessage=mymessage.Replace(',', '.');
									ModifiedValues.Add(mymessage); //X
									if (messages.Count>1)
										mymessage=Convert.ToString(messages[1]);
									else
										mymessage=Convert.ToString(messages[0]);
									mymessage=mymessage.Replace(',', '.');
									 ModifiedValues.Add(mymessage); //Y
									ModifiedValues.Add("0"); //Z
									ModifiedValues.Add(localstate);
									break;
								case "Y":
									ModifiedValues.Add("0"); //X
									mymessage=Convert.ToString(messages[0]);
									mymessage=mymessage.Replace(',', '.');
									ModifiedValues.Add(mymessage); //Y
									ModifiedValues.Add("0"); //Z
									ModifiedValues.Add(localstate);
									break;								
							}	
					} 						
					else
						Flogger.Log(LogType.Debug, "packet is not okay.");

				}
	
			
		}

		
		


		private void CheckIt()
		{
			for (int i=0; i<FTypeTag.SliceCount; i++)
			{
				if (FTypeTag[i].Contains("f"))
				{
					switch (FTypeTag[i])
					{
						case "ffff":
							UnmodfifiedOSCMessages.Add(FOSCMessages[i]);
							break;
						
						case "f":
							CompleteOSCMessage(i,FExpectedIOType[i]);						
							break;
						
						case "ff":
							CompleteOSCMessage(i,FExpectedIOType[i]);
						break;

						case "fff":
							CompleteOSCMessage(i,FExpectedIOType[i]);
							break;
					}
					
				}
				else
				UnmodfifiedOSCMessages.Add(FOSCMessages[i]);

			}
				
		}



		public void Evaluate(int SpreadMax)
		{
			UnmodfifiedOSCMessages.Clear();
			ModifiedOSCAdress.Clear();
			ModifiedValues.Clear();
			
			if (FOSCMessages.SliceCount > 0 && FOSCMessages.IsChanged) 
			{				
					CheckIt();				
			}
			

			FUnmodifiedOSCMessages.AssignFrom(UnmodfifiedOSCMessages);
			FModifiedOSCAdress.AssignFrom(ModifiedOSCAdress);
			FModifiedValues.AssignFrom(ModifiedValues);
			
	
			
			
			
		}
	}
}
