#region usings
using System;
using System.ComponentModel.Composition;
using System.Collections.Generic;

using VVVV.PluginInterfaces.V1;
using VVVV.PluginInterfaces.V2;
using VVVV.Utils.VColor;
using VVVV.Utils.VMath;

using VVVV.Core.Logging;
#endregion usings

namespace VVVV.Nodes
{
	#region PluginInfo
	[PluginInfo(Name = "HyperDictionary", Category = "String", Help = "stores spreads of strings assigned to an ID", Tags = "String Buffer ID S+H Queue")]
	#endregion PluginInfo
	public class StringDictionaryNode : IPluginEvaluate
	{
		#region fields & pins
		[Input("SetID")]
		ISpread<string> FSetID;

		[Input ("InputString")]
		ISpread<ISpread<string>> FInputString;
		
		[Input ("GetID")]
		ISpread<string> FGetID;
				
		[Input("ClearBuffer", IsBang = true, IsSingle = true)]
		ISpread<bool> FClearBuffer;
		
		[Input ("Default")]
		ISpread<ISpread<string>> FDefaultString;
		
		[Input("Allow Adding", IsSingle = true, DefaultValue=1)]
		ISpread<bool> FAllowAdding;
	
		[Input("Dump", IsBang = true, IsSingle = true)]
		ISpread<bool> FDump;

		
		
		[Output ("OutputValue")]
		ISpread<ISpread<string>> FOutputString;
						
		[Output ("OutputDumpID")]
		ISpread<string> FOutputDumpID;
		
		[Output ("OutputDumpValue")]
		ISpread<ISpread<string>> FOutputDump;
		
		[Output ("Key Count")]
		ISpread<double> FOutputKeys;
		
		[Import()]
		ILogger FLogger;
		
		Dictionary<string, ISpread<string>> d = new Dictionary <string, ISpread<string>>();
		#endregion fields & pins
		public void MkDict()	
		{
			int count;				
			count = Math.Min(FInputString.SliceCount, FSetID.SliceCount);

			if (count!= 0)
			{
				for (int i=0; i<FInputString.SliceCount; i++)
				{
	
					if (FAllowAdding[0])
					{
						if(d.ContainsKey(FSetID[i]))
							d.Remove(FSetID[i]);		
						d.Add(FSetID[i], FInputString[i].Clone());	
					
					}
					else
						if(d.ContainsKey(FSetID[i]))
						{
							d.Remove(FSetID[i]);					
							d.Add(FSetID[i], FInputString[i].Clone());		 	
						}
				}
		}
			
		}
		public void Evaluate(int SpreadMax)
		{
			MkDict();
			if (FClearBuffer[0])
				d.Clear();
			
			
			
			FOutputString.SliceCount=FGetID.SliceCount;
			FOutputDumpID.SliceCount=d.Count;
			FOutputDump.SliceCount=d.Count;	
			FOutputKeys[0] = d.Count; 
			
			for (int i=0; i<FGetID.SliceCount; i++)
			{
				if (d.ContainsKey(FGetID[i]))
				FOutputString[i] = d[FGetID[i]];
			else
				FOutputString[i] = FDefaultString[i];
			}	
			if (FDump[0])
			{
				int j=0;
				foreach (ISpread<string> v in d.Values)
				{
					FOutputDump[j] = v;
					j++;
				}
				j=0;
				foreach (string v in d.Keys)
				{
					FOutputDumpID[j]=v.ToString();
					j++;
				}
				
			}
    		
		}
	}
}