#region usings
using System;
using System.ComponentModel.Composition;
using System.Globalization;
using System.Collections.Generic;
using System.Collections;
using VVVV.PluginInterfaces.V1;
using VVVV.PluginInterfaces.V2;
using VVVV.Utils.VMath;

using VVVV.Core.Logging;
#endregion usings

namespace VVVV.Nodes
{



	#region PluginInfo
	[PluginInfo(Name = "HasLetter", Category = "String", Help = "Checks whether string contains alphanumeric Characters", Tags = "Filter, Analyze")]
	#endregion PluginInfo

	public class StringHasLetterNode : IPluginEvaluate
	{

		#region fields & pins


		[Input("Input", DefaultString = "text")]
		IDiffSpread<string> FInput;


//-------------------------------------


		[Output("Contains Alphanumeric Characters")]
		ISpread<bool> FOutputContainsAlphanumericCharacters;


		[Import()]
		IPluginHost FHost;


		#endregion fields & pins


		public void Evaluate(int SpreadMax)
		{
			FOutputContainsAlphanumericCharacters.SliceCount = FInput.SliceCount;
			if (FInput.IsChanged && FInput.SliceCount > 0) {
				for (int i = 0; i < FInput.SliceCount; i++) {
					for (int j = 0; j < FInput[i].Length; j++) {
						if (Char.IsLetter(FInput[i], j)) {
							FOutputContainsAlphanumericCharacters[i] = true;
							break;
						} else
							FOutputContainsAlphanumericCharacters[i] = false;


					}

				}


			}



		}
	}
}
