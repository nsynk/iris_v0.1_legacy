#region usings
using System;
//using System.Collections.Generic;
//using Microsoft.CSharp;
//using System.Linq;
using System.ComponentModel.Composition;
//using Microsoft.CSharp.RuntimeBinder;
//using System.Xml;
using System.Xml.Linq;
using VVVV.PluginInterfaces.V1;
using VVVV.PluginInterfaces.V2;
//using VVVV.Utils.VColor;
//using VVVV.Utils.VMath;
//using AmazedSaint.Elastic;
//using AmazedSaint;
using VVVV.Core.Logging;
using AmazedSaint.Elastic.Lib;
#endregion usings

namespace VVVV.Nodes
{
	#region PluginInfo
	[PluginInfo(Name = "IrisConfigXML", Category = "XML", Help = "Basic template with one string in/out", Tags = "")]
	#endregion PluginInfo
	public class XMLIrisConfigXMLNode : IPluginEvaluate
	{

		[Input("Server Resolution")]
		ISpread<int> FServerResolution;

		[Input("Client Resolution")]
		ISpread<int> FClientResolution;

		[Input("MasterIP")]
		ISpread<string> FMasterIP;		
	
		[Input("Broadcast IP")]
		ISpread<string> FBroadcastIP;

		[Input("Client IP´s")]
		ISpread<string> FClientIP;

		[Input("Client PerfMeter On")]
		ISpread<bool> FClientPerfMeterOn;

		[Input("Client Fullscreen")]
		ISpread<bool> FClientFullscreen;
		
		[Input("Work without Network")]
		ISpread<bool> FWorkWithoutNetwork;
		
		[Input("Preset File Location")]
		ISpread<string> FPresetFileLocation;

		
		
		
		[Input("Enable", IsBang = true, IsSingle = true)]
		ISpread<bool> FEnable;

		[Output("Output")]
		ISpread<string> FOutput;

		[Output("Debug")]
		ISpread<string> FDebug;

		[Import()]
		ILogger FLogger;


		//called when data for any output pin is requested




	
//see source: http://www.amazedsaint.com/2010/02/introducing-elasticobject-for-net-40.html
		public void Evaluate(int SpreadMax)
		{
		string temp_clientIPs="";
			if (FEnable[0]) {
				dynamic irisconfig = new ElasticObject("IRISConfig");	
			
				for (int i=0; i<FClientIP.SliceCount; i++)
				{
					temp_clientIPs += FClientIP[i];
					if (i< FClientIP.SliceCount - 1)
						temp_clientIPs += ",";
				}
				
			
				irisconfig.ServerResolution.value=Convert.ToString((FServerResolution[0])+","+Convert.ToString(FServerResolution[1]));
				irisconfig.ClientResolution.value=Convert.ToString((FClientResolution[0])+","+Convert.ToString(FClientResolution[1]));
				irisconfig.MasterIP.value = FMasterIP[0];
				irisconfig.BroadcastIP.value=FBroadcastIP[0];
				irisconfig.ClientIPs.value=temp_clientIPs;
				irisconfig.ClientPerfmeterOn.value=FClientPerfMeterOn[0];
				irisconfig.ClientFullscreenOn.value=FClientFullscreen[0];
				irisconfig.WorkWithoutNetwork.value=FWorkWithoutNetwork[0];	
				irisconfig.PresetFileLocation.value=FPresetFileLocation[0];
			
//				var Path = Slot << "Path";
//				Slot.Path= FPatchesPath[i];

					
				
				XElement el = irisconfig > FormatType.Xml;
				FOutput.SliceCount = 1;
				FOutput[0] = Convert.ToString(el);
				}//FLogger.Log(LogType.Debug, "Logging to Renderer (TTY)");

			

		}

		}
	}

